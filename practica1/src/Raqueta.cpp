//Autor David Roncero
// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1=y1+velocidad.y*t;
	y2=y2+velocidad.y*t;
}

void Raqueta::disminuye()
{
	y2=y2-(y2-y1)/10;
}

void Raqueta::resetTamano()
{
	y1=-1;	
	y2=1;
}
