// Mundo.cpp: implementation of the CMundoServidor class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
void* hilo_comandos(void* d)
{
      CMundoServidor* p=(CMundoServidor*) d;
      p->RecibeComandosJugador();
}


CMundoServidor::CMundoServidor()
{
	Init();
}

CMundoServidor::~CMundoServidor()
{
//	close(fd);
//	munmap(proyeccion,sizeof(datosMemoria));

	char cadFin[16];
	sprintf(cadFin,"Fin del juego\n");

	write(fd,cadFin,strlen(cadFin)+1);
	close(fd);

	write(fd_coordenadas,cadFin,strlen(cadFin)+1);
	close(fd_coordenadas);

	write(fd_teclas,cadFin,strlen(cadFin)+1);
	close(fd_teclas);
	
}

void CMundoServidor::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoServidor::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cade[100];
	sprintf(cade,"Jugador1: %d",puntos1);
	print(cade,10,0,1,1,1);
	sprintf(cade,"Jugador2: %d",puntos2);
	print(cade,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoServidor::OnTimer(int value)
{	
/*	pDatosMemoria->esfera=esfera;
	pDatosMemoria->raqueta1=jugador1;
	pDatosMemoria->raqueta2=jugador2;
	
	switch(pDatosMemoria->accion)
	{
		case 1: jugador1.velocidad.y=4;break;
		case 0: break;
		case -1: jugador1.velocidad.y=-4;break;
	}
	t_aux=t;
	t_aux=clock()-t;
	if( (float(t_aux)/CLOCKS_PER_SEC) >= 0.5 )
	{
	switch(pDatosMemoria->accion2)
	{
		case 1: jugador2.velocidad.y=4;break;
		case 0: break;
		case -1: jugador2.velocidad.y=-4;break;
	}
	}
*/

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	if(jugador1.Rebota(esfera))jugador2.disminuye();
	if(jugador2.Rebota(esfera))jugador1.disminuye();
	if(fondo_izq.Rebota(esfera))
	{
		esfera.resetRadio();
		jugador1.resetTamano();
		jugador2.resetTamano();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		sprintf(cad,"Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos2);
		write(fd,cad,strlen(cad)+1);
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.resetRadio();
		jugador1.resetTamano();
		jugador2.resetTamano();
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		sprintf(cad,"Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos1);
		write(fd,cad,strlen(cad)+1);
	}
	
	sprintf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", esfera.centro.x,esfera.centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2, jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);

	write(fd_coordenadas,cad,strlen(cad)+1);

}

void CMundoServidor::OnKeyboardDown(unsigned char key, int x, int y)
{
/*	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':if(jugador1.velocidad.y!=0) jugador1.velocidad.y=0;
		else jugador1.velocidad.y=-4;break;
	case 'w':if(jugador1.velocidad.y!=0)jugador1.velocidad.y=0;
		else jugador1.velocidad.y=4;break;
	case 'l'://t=clock();
		if(jugador2.velocidad.y!=0)jugador2.velocidad.y=0;
		else jugador2.velocidad.y=-4;break;
	case 'o'://t=clock();
		if(jugador2.velocidad.y!=0)jugador2.velocidad.y=0;
		else jugador2.velocidad.y=4;break;

	}
*/
}

void CMundoServidor::Init()
{
	//ABRIR FIFO LOGGER	
	fd=open("/tmp/FifoLogger",O_WRONLY);
	if(fd==-1)
	{
		perror("\nError al abrir FifoLogger\n");
		exit(1);
	}

	
/*
	fdMemoria=open("/tmp/datosBot.txt", O_RDWR|O_CREAT|O_TRUNC, 0777);
	write(fdMemoria, &datosMemoria, sizeof(datosMemoria));
	proyeccion=(char*)mmap(NULL,sizeof(datosMemoria),PROT_WRITE|PROT_READ,MAP_SHARED,fdMemoria,0);
	close(fdMemoria);
	pDatosMemoria=(DatosMemoriaCompartida*)proyeccion;
	pDatosMemoria->accion=0;
*/	
	

	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	
	//ABRIR FIFO COORDENADAS
	fd_coordenadas=open("/tmp/FifoCoordenadas",O_WRONLY);
	if(fd_coordenadas==-1)
	{
		perror("\nError al abrir FifoCordenadas\n");
		exit(1);
	}

	//ABRIR FIFO TECLAS
	fd_teclas=open("/tmp/FifoTeclas",O_RDONLY);
	if(fd_teclas==-1)
	{
		perror("\nError al abrir FifoTeclas\n");
		exit(1);
	}
	pthread_create(&thr,NULL,hilo_comandos,this);
}


void CMundoServidor::RecibeComandosJugador()
{
	while (1) {
		usleep(10);
        	char cade[100];
        	read(fd_teclas, cade, sizeof(cade));
        	unsigned char key;
            
		sscanf(cade,"%c",&key);
		if(cade[0]=='F')exit(1);
		else
        		if(key=='s'){
				if(jugador1.velocidad.y!=0) jugador1.velocidad.y=0;
				else jugador1.velocidad.y=-4;
			}
        		if(key=='w'){
				if(jugador1.velocidad.y!=0)jugador1.velocidad.y=0;
				else jugador1.velocidad.y=4;
			}
        		if(key=='l'){
				if(jugador2.velocidad.y!=0)jugador2.velocidad.y=0;
				else jugador2.velocidad.y=-4;
			}
        		if(key=='o'){
				if(jugador2.velocidad.y!=0)jugador2.velocidad.y=0;
				else jugador2.velocidad.y=4;
			}
	}
}


